# Projekt byl sloučen s původním kódem
Je možné dále používat původní [Ulozto-Downloader](https://github.com/setnicka/ulozto-downloader)
Veškeré změny jsou plně [zahrnuty](https://github.com/setnicka/ulozto-downloader/pull/50).

# Je tedy prohlášeno jako opuštěné a zastaralé od 22.5.2021 !
